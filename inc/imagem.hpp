#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <bits/stdc++.h>

using namespace std;
class Imagem{

private:
	int largura;
	int altura;
	int max;
	string entrada;
	string saida;
	string formato; // P2, P3, P5 ou P6

public:
	Imagem();
	~Imagem();
	void setLargura(int largura);
	int getLargura();
	void setAltura(int altura);
	int getAltura();
	void setMax(int max);
	int getMax();
	void setEntrada(string entrada);
	string getEntrada();
	void setSaida(string saida);
	string getSaida();
	void setFormato(string formato);
	string getFormato();
	//Retorna 1 caso a imagem for pgm e 2 caso a imagem for ppm
	//Retorna 0 caso não for nenhuma das 2
	int identifica();
};
#endif
