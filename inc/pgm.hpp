#ifndef PGM_HPP
#define PGM_HPP
#include <bits/stdc++.h>
#include "imagem.hpp"

using namespace std;

class Pgm : public Imagem{
private:
	vector<int> imgDec;
	vector <char> imgAscii;
	int tipo; // 2 "P2" decimal 5 "P5" Ascii
	string mensagem;
	int inicio, duracao, n; //Instruções do enigma

public:
	Pgm();
	Pgm(string entrada, string saida);
	~Pgm();

	void imprimeDados();
	string getMensagem();
	void criaArquivo();
	void leitura();
	//Função recebe a linha do comentário e coleta as informaçoes do enigma
	void getInstrucoes(char * linha);
	string decrip();

};
#endif
