#ifndef PPM_HPP
#define PPM_HPP
#include <bits/stdc++.h>
#include "imagem.hpp"

using namespace std;

class Ppm : public Imagem{
public:
	struct px{
		int r, g, b; //Variáveis caso a matriz seja Decimal
		unsigned char A; //Variáveis caso a matriz seja Ascii
	};
private:
	vector<struct px> img;
	int tipo; //3 "P3" decimal 6 "P6" Ascii
	//Instruções do enigma
	int inicio, duracao;
	string palavra;
	string mensagem;

public:
	Ppm();
	Ppm(int largura, int altura, int max, string entrada, string saida);
	~Ppm();

	void leitura();
	void criaArquivo();
	void imprimeDados();
	void getInstrucoes(char * linha);
	string getMensagem();
	void decrip();

};
#endif
