#include "pgm.hpp"
#include "ppm.hpp"
#include "imagem.hpp"
#include <bits/stdc++.h>
#include <stdlib.h>
int menu();
using namespace std;

int main(){
	//Início do progama
	system("clear");
	cout << "Exercício de programação 1\n";
	cout << "Gutavo Marques Lima - 17/0035158\n";
	cout << "Aperte enter para continuar...";
	getchar();
	string caminho;
	int n;
	Ppm * ppm1 = new Ppm();
	Pgm * pgm1 = new Pgm();
	Imagem aux;
	//Menu
	while(true){
		system("clear");
		cout << "Imagem aberta: \""<< caminho <<"\"\n";
		switch(menu()){
			case 1:
				cout << "Selecione o diretório da imagem:\n";
				cout << "Ex:/img/exemplo.pgm\n";
				cout << "/";
				cin >> caminho;
				getchar();
				aux.setEntrada(caminho);
				n=aux.identifica();
				if(n==0){
					cout << "Entrada inválida pressione enter para voltar ao menu...";
					getchar();
				}
				else if(n==2){
					ppm1-> setEntrada(caminho);
					ppm1-> leitura();
					cout << "Pressione enter para voltar ao menu...";
					getchar();
				}
				else{
					pgm1-> setEntrada(caminho);
					pgm1-> leitura();
					cout << "Pressione enter para voltar ao menu...";
					getchar();
				}
			break;
			case 2:
				cout << "Selecione o diretório para cópia da imagem:\n";
				cout << "Ex:/img/exemplo.ppm\n";
				cout << "/";
				cin >> caminho;
				getchar();
				if(n==0){
					cout << "Entrada inválida pressione enter para voltar ao menu...";
					getchar();
				}
				else if(n==2){
					ppm1-> setSaida(caminho);
					ppm1-> criaArquivo();
					cout << "Pressione enter para voltar ao menu...";
					getchar();
				}
				else{
					pgm1-> setSaida(caminho);
					pgm1-> criaArquivo();
					cout << "Pressi&&one enter para voltar ao menu...";
					getchar();
				}
			break;
			case 3:
				if(n==1){
					cout << "Caso não apareça nada, não foi possível achar uma mensagem criptografada\n";
					cout << "Mensagem: \"" << pgm1 -> getMensagem() << "\"\n";
					cout << "Pressione enter para voltar ao menu...";
					getchar();
				}
				else if(n==2){
					cout << "Mensagem:\"" << ppm1 -> getMensagem() << "\"\n";
					cout << "Pressione enter para voltar ao menu...";
					getchar();
				}
				else{
					cout << "É preciso abrir uma imagem para descriptografala\n";
					cout << "Pressione enter para voltar ao menu...";
					getchar();
				}
			break;
			case 4:
				delete ppm1;
				delete pgm1;
			return 0;
		}
	}
}
int menu(){
	int opcao;
	cout << "1 - Abrir uma imagem\n";
	cout << "2 - Salvar cópia da imagem\n";
	cout << "3 - Obter mensagem criptografada\n";
	cout << "4 - Sair\n";
	while (true){
		cin >> opcao;
		if(opcao<1 || opcao>4){
			cout << "Entrada inválida, entre com um valor de 1 a 4: ";
		}
		else{
			getchar();
			return opcao;
		}
	}
}
