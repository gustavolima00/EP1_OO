#include "imagem.hpp"
#include "ppm.hpp"
#include <bits/stdc++.h>

using namespace std;

Ppm::Ppm(){
	setLargura(1);
	setAltura(1);
	setMax(1);
	setEntrada("");
	setSaida("out.ppm");
	inicio = 0;
	duracao = 0;
	palavra = "";
}

Ppm::Ppm(int largura, int altura, int max, string entrada, string saida){
	setLargura(largura);
	setAltura(altura);
	setMax(max);
	setEntrada(entrada);
	setSaida(saida);
}

Ppm::~Ppm(){
}

void Ppm::imprimeDados(){
	cout << "Formato PPM\n";
	cout << "Largura: " << getLargura() << endl;
  cout << "Altura: " << getAltura() << endl;
	cout << "Entrada:" << getEntrada() << endl;
}

void Ppm::criaArquivo(){
	//Criação do arquivo
	FILE *arq;
	arq = fopen(getSaida().c_str(), "wt");
	if (arq == NULL){
  	printf("Problemas na CRIACAO do arquivo\n");
  	return;
	}
	//Impressão do cabeçalho
	fprintf(arq, "P%d\n", tipo);
	fprintf(arq, "%d %d\n", getLargura(), getAltura());
	fprintf(arq, "%d\n", getMax());

	//Impressão da matriz
	if(tipo==3){
		int aux=0;
		for(auto px:img){
			if(aux==20){
				fprintf(arq, "\n");
				aux=0;
		 	}
			fprintf(arq, "%d %d %d\t\n", px.r, px.g, px.b);
			aux++;
		}
		fprintf(arq, "\n");
	}
	else{
		for(auto px:img){
			fprintf(arq, "%c", px.A);
		}
	}
	cout << "Arquivo PPM criado em:" << getSaida() << endl;
  fclose(arq);
}

void Ppm::leitura(){
	//Abertura do arquivo a ser lido
	FILE *arq;
	arq = fopen(getEntrada().c_str(), "rt");
	if (arq == NULL){
     printf("Problemas na abertura do arquivo\n");
     return;
  }
	int n=100; //Número máximo de caracteres na linha
	char linha[n];

	//Leitura da primeira linha
	if(!(fgets(linha, n, arq))){
		printf("Não foi possível ler o arquivo"); //Erro de fim de arquivo
		return;
	}
	int tipo;
	sscanf(linha,"P%d", &tipo);
	this->tipo=tipo;
	if(tipo!=3 && tipo!=6){
		printf("Esse arquivo não é PPM\n");
		return;
	}

	//Leitura da segunda linha Largura e Altura
	// Procura do comentário com Instruções do enigma
	do{
		if(!(fgets(linha, n, arq))){
			printf("Não foi possível ler o arquivo"); //Erro de fim de arquivo
			return;
		}
		getInstrucoes(linha);
	}while(linha[0]=='#');
	int largura, altura;
	sscanf(linha,"%d %d", &largura, &altura);
	setLargura(largura);	setAltura(altura);

	//Leitura da terceira linha Maior pixel da imagem
	if(!(fgets(linha, n, arq))){
		printf("Não foi possível ler o arquivo"); //Erro de fim de arquivo
		return;
	}
	int max;
	sscanf(linha,"%d", &max);
	setMax(max);

	//Leitura da matriz da imagem
	struct px x;
	img.clear();
	//Caso a imagem seja decimal
	if(tipo==3){
		for(int i=0; i<altura; ++i){
			for(int j=0; j<largura; j++){
				if((fscanf(arq,"%d%d%d", &x.r, &x.g, &x.b)==EOF)){
					printf("Não foi possível ler o arquivo"); //Erro de fim de arquivo
					return;
				}
				img.push_back(x);
			}
		}
	}
	//Caso a imagem seja ASCII
	else{
		for(int i=0; i<altura; ++i){
			for(int j=0; j<largura*3; j++){
				if((fscanf(arq,"%c", &x.A)==EOF)){
					printf("Não foi possível ler o arquivo"); //Erro de fim de arquivo
					return;
				}
				img.push_back(x);
			}
		}

	}
	decrip();
	fclose(arq);
	printf("Leitura efetuada com sucesso\n");
}
void Ppm::getInstrucoes(char * linha){
	if(linha[0]!='#')
		return;

	//Verificação se há apenas números em seguida
	for(int i=1; linha[i]!=' ' ;i++){
		if(linha[i]!=' '&& linha[i]!='\t' && linha[i]<'0' && linha[i]>'9')
			return;
	}
	int inicio, duracao;
	char palavra[26];
	sscanf(linha,"#%d%d %s", &inicio, &duracao, palavra);
	this -> inicio=inicio;
	this -> duracao=duracao;
	this -> palavra=palavra;

}
void Ppm::decrip(){
	if(tipo==3){
		return;
	}
	else if(tipo==6){
		//Armazenamento da mensagem em um vetor de int
		vector <int> texto;
		for(int i=inicio; i<inicio+(duracao*3); i+=3){
			int a=img[i].A, b=img[i+1].A, c=img[i+2].A;
			texto.push_back((a%10)+(b%10)+(c%10));
		}

		//Dicionário do alfabeto
		//Começo do Dicionário	
		char palavra[26];
		strcpy(palavra, this->palavra.c_str());
		set<char> alfabeto;
		map<char, int> dicionario;
		int i, j, tam=strlen(palavra);
		for(i=1; i<=tam; i++){
			if(palavra[i-1]>='a'&&palavra[i-1]<='z'){
				alfabeto.insert(palavra[i-1]-32);
				dicionario[palavra[i-1]-32]=i;
			}
			else if(palavra[i-1]>='A'&&palavra[i-1]<='A'){
				alfabeto.insert(palavra[i-1]);
				dicionario[palavra[i-1]]=i;
			}
		}
		//Final do dicionário
		for(j=0; j<26; j++){
			if (alfabeto.count('A'+j) != 0){
		    continue;
			}
			dicionario['A'+j]=i;
			i++;
		}
		//Decriptografia da mensagem
		char mensagem[duracao];
		for(int i=0; i<duracao; i++){
			if(texto[i]==0)
				mensagem[i]=' ';
			else
				mensagem[i]='A'+dicionario['A'+texto[i]-1]-1;
		}
		mensagem[duracao]='\0';
		
		this->mensagem=mensagem;
	}
}
string Ppm::getMensagem(){
	 return mensagem;
}
