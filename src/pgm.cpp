#include "imagem.hpp"
#include "pgm.hpp"
#include <bits/stdc++.h>

using namespace std;

Pgm::Pgm(){
	setLargura(1);
	setAltura(1);
	setMax(1);
	setEntrada("");
	setSaida("out.pgm");
	mensagem="";
	inicio=0;
	duracao=0;
	n=0;
}

Pgm::Pgm(string entrada, string saida){
	setEntrada(entrada);
	setSaida(saida);
}

Pgm::~Pgm(){
}

void Pgm::imprimeDados(){
	cout << "Formato PGM\n";
	cout << "Largura: " << getLargura() << endl;
  cout << "Altura: " << getAltura() << endl;
	cout << "Local:" << getEntrada() << endl;
}

string Pgm::getMensagem(){
	return mensagem;
}

void Pgm::criaArquivo(){
	//Criação do arquivo
	FILE *arq;
	arq = fopen(getSaida().c_str(), "wt");
	if (arq == NULL){
  	printf("Problemas na CRIAÇÃO do arquivo\n");
  	return;
	}
	//Impressão do cabeçalho
	fprintf(arq,"P%d\n", tipo);
	fprintf(arq, "%d %d\n", getLargura(), getAltura());
	fprintf(arq, "%d\n", getMax());

	//Impressão da matriz
	//Caso seja decimal
	if(tipo==2){
		int l=getLargura();
		int aux=0;
		for(auto px:imgDec){
			if(aux==l){
				fprintf(arq,"\n");
				aux=0;
			}
			fprintf(arq,"%d\t",px);
			aux++;
		}
		fprintf(arq,"\n");
	}
	//Caso seja ASCII
	else{
		for(auto px:imgAscii){
			fprintf(arq,"%c",px);
		}
	}
  fclose(arq);
	cout << "Arquivo PGM criadao em :" << getSaida() << endl;
}

void Pgm::leitura(){
	//Abertura do arquivo a ser lido
	FILE *arq;
	arq = fopen(getEntrada().c_str(), "rt");
	if (arq == NULL){
		 printf("Problemas na abertura do arquivo\n");
		 return;
	}
	int n=100; //Número máximo de caracteres na linha
	char linha[n];

	//Procura de comentário
	if(!(fgets(linha, n, arq))){
		printf("Não foi possível ler o arquivo completamente\n"); //Erro de fim de arquivo
		return;
	}
	//Leitura da primeira linha
	int tipo;
	sscanf(linha,"P%d", &tipo);
	this->tipo=tipo;
	if(tipo!=2 && tipo!=5){
		printf("Esse arquivo não é PGM\n");
		return;
	}

	//Procura do comentário com inicio duração e o n
	do{
		if(!(fgets(linha, n, arq))){
			printf("Não foi possível ler o arquivo completamente\n"); //Erro de fim de arquivo
			return;
		}
		getInstrucoes(linha);
	}while(linha[0]=='#');
	//Leitura da segunda linha Largura e Altura
	int largura, altura;
	sscanf(linha,"%d %d", &largura, &altura);
	setLargura(largura);	setAltura(altura);

	//Procura de comentário
	if(!(fgets(linha, n, arq))){
		printf("Não foi possível ler o arquivo completamente\n"); //Erro de fim de arquivo
		return;
	}
	//Leitura da terceira linha Maior pixel da imagem
	int max;
	sscanf(linha,"%d", &max);
	setMax(max);

	//Leitura da matriz da imagem

	//Caso seja decimal
	if(tipo==2){
		int px;
		imgDec.clear();
		for(int i=0; i<altura; ++i){
			for(int j=0; j<largura; j++){
					if(fscanf(arq,"%d", &px)==EOF){
						printf("Não foi possível ler o arquivo completamente\n"); //Erro de fim de arquivo
						return;
					}
				imgDec.push_back(px);
			}
		}
	}
	//Caso seja ASCII
	else{
		imgAscii.clear();
		char px;
		for(int i=0; i<altura; ++i){
			for(int j=0; j<largura; j++){
					if(fscanf(arq,"%c", &px)==EOF){
						printf("Não foi possível ler o arquivo completamente\n"); //Erro de fim de arquivo
						return;
					}
				imgAscii.push_back(px);
			}
		}

	}
	mensagem = decrip(); //decriptografia da mensagem
	fclose(arq);
	printf("Leitura efetuada com sucesso\n");
}
void Pgm::getInstrucoes(char * linha){
	if(linha[0]!='#')
		return;

	//Verificação se há apenas números em seguida
	for(int i=1; linha[i]!='\n' ;i++){
		if(linha[i]!=' '&& linha[i]!='\t' && linha[i]<'0' && linha[i]>'9')
			return;
	}
	int inicio, duracao, n;
	sscanf(linha,"#%d%d%d", &inicio, &duracao, &n);
	this -> inicio=inicio;
	this -> duracao=duracao;
	this -> n=n;

}

string Pgm::decrip(){
  char msg[duracao+1];
  n%=26;
  map<char, char> alfabeto;
  //Conversão das letras que não voltam para o Z
	alfabeto[' ']=' ';
  for(int i=n; i<26; i++){
    alfabeto['A'+i]='A'+i-n;
    alfabeto['a'+i]='a'+i-n;
  }

  //Conversão das letras que voltam para o Z
  for(int i=0; i<n; i++){
    alfabeto['A'+(n-1)-i]='Z'-i;
    alfabeto['a'+(n-1)-i]='z'-i;
  }

  //Decriptografia  da mensagem
  for(int i=inicio, j=0; i<(inicio+duracao); i++, j++){
		if (alfabeto.find(imgAscii[i]) != alfabeto.end())
    	msg[j]=alfabeto[imgAscii[i]];
		else
			msg[j]=imgAscii[i];
  }
	msg[duracao]='\0';
  string str=msg;
  return str;
}
