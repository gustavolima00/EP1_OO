#include "imagem.hpp"
#include <bits/stdc++.h>

using namespace std;

Imagem::Imagem(){
	largura = 1;
	altura = 1;
	max = 1;
	entrada = "";
	saida = "a.out";
}
Imagem::~Imagem(){
}
void Imagem::setLargura(int largura){
	this->largura = largura;
}
int Imagem::getLargura(){
	return largura;
}
void Imagem::setAltura(int altura){
	this->altura = altura;
}
int Imagem::getAltura(){
	return altura;
}
void Imagem::setMax(int max){
	this->max=max;
}
int Imagem::getMax(){
	return max;
}
void Imagem::setEntrada(string entrada){
	this->entrada=entrada;
}
string Imagem::getEntrada(){
	return entrada;
}
void Imagem::setSaida(string saida){
	this->saida=saida;
}
string Imagem::getSaida(){
	return saida;
}
void Imagem::setFormato(string formato){
	this->formato=formato;
}
string Imagem::getFormato(){
	return formato;
}
int Imagem::identifica(){
	//Abertura do arquivo a ser lido
	FILE *arq;
	arq = fopen(getEntrada().c_str(), "rt");
	if (arq == NULL){
		 printf("Problemas na abertura do arquivo\n");
		 return 0;
	}
	int tipo;
	if(!(fscanf(arq,"P%d", &tipo))){
		printf("Não foi possível ler o arquivo completamente\n"); //Erro de fim de arquivo
		return 0;
	}
	if(tipo==2 || tipo==5)
		return 1;
	else if(tipo==3 || tipo==6)
		return 2;
	else
		return 0;
	fclose(arq);
}
