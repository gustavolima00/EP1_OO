###INFORMAÇOES###
- Gustavo Marques Lima
- 17/0035158

###INSTRUÇÕES###

#DOWNLOAD DO PROJETO 
- Abra o terminal no diretório desejado e execute o comando:
 
 - $ git clone https://gitlab.com/gustavolima00/EP1_OO

#EXECUÇÃO DO PROJETO
- Abra o terminal no diretorio raiz do projeto (O diretório desse arquivo README)
- Copie as imagens para um dos diretórios do projeto (Você tambem pode criar seu proprio diretório)
- No terminal execute os comantos:

 - $ make
 - $ make run

- Na execução do código Aparecerá um menu no qual aceita valores de 1 a 4.
- Opção 1 abre uma imagem para leitura. É preciso indicar o diretório localizado a imagem.
- Opção 2 cria uma cópia de uma imagem lida pelo progama.
- Opção 3 mostra no terminal a mensagem escondida na imagem.
- Opção 4 fecha o progama.


